# Create your views here.
from django.contrib.auth.models import User
import pytz
from django.conf import settings
from CalgarySecularChurch.tools import get_time_now
from ladder.models import BLOOD_DRIVE, Attendance, Achievement,\
    TOTAL_BLOOD_DRIVE, SUNDAY_MEETING, CONSECUTIVE_SUNDAY_MEETINGS, Donation,\
    ANNUAL_DONATIONS, Event, HOME_CHURCH, HOME_CHURCH_HOST
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import permission_required
from datetime import timedelta, date
from django.utils.datetime_safe import datetime

MEETING_LOWER_BOUND = 22
MEETING_UPPER_BOUND = 35


def member(request, member_id):
    member = User.objects.get(id=member_id)
    achievements = Achievement.objects.filter(member=member).order_by('name', 'level')
    attendances = Attendance.objects.filter(member=member)
    donations = Donation.objects.filter(member=member).order_by('-datetime')
    return render_to_response('ladder/member.html', locals(),
        context_instance=RequestContext(request))


@permission_required('ladder.can_take_attendance')
def event_selector(request, member_id):
    this_morning = get_time_now().today() - timedelta(days=1)
    tonight = get_time_now().today() + timedelta(days=1)
    current_events = Event.objects.filter(datetime__gte=this_morning,
        datetime__lte=tonight)
    member = User.objects.get(id=member_id)
    return render_to_response('ladder/event_selector.html', locals(),
        context_instance=RequestContext(request))


@permission_required('ladder.can_take_attendance')
def attendance(request, member_id, event_id):
    member = User.objects.get(pk=member_id)
    event = Event.objects.get(pk=event_id)
    Attendance.objects.get_or_create(member=member, event=event)
    member.profile.add_points(event.points())
    calculate_achievements(member)
    return render_to_response('ladder/attendance.html',
        context_instance=RequestContext(request))


def calculate_achievements(member):
    achievement_blood_drive(member)
    achievement_consecutive_sunday_meetings(member)
    achievement_donation(member)
    achievement_home_church_host(member)


def achievement_donation(member):
    start_of_year = datetime(year=date.today().year, month=1, day=1, hour=0, minute=0,
        second=0)
    donations = Donation.objects.filter(member=member, datetime__gte=start_of_year)
    total = 0
    for donation in donations:
        total += donation.amount
    create_achievements(total, ANNUAL_DONATIONS, member)


def achievement_consecutive_sunday_meetings(member):
    meetings = Attendance.objects.filter(member=member,
        event__description=SUNDAY_MEETING).order_by('event__datetime')
    consecutive = 0
    captured = 0
    for x in range(len(meetings)):
        if x == 0:
            consecutive = 1
            captured = 1
        else:
            if MEETING_LOWER_BOUND < \
                    timedelta(meetings[x].datetime - meetings[x-1].datetime) < \
                    MEETING_UPPER_BOUND:
                consecutive += 1
            else:
                if consecutive > captured:
                    captured = consecutive
                consecutive = 0
    create_achievements(captured, CONSECUTIVE_SUNDAY_MEETINGS,
        member)

def create_achievements(number, event_type, member):
    if number > 0:
        top_level = 1
        created = True
        while (top_level < number):
            top_level = top_level * 2
            if top_level > number:
                top_level = top_level / 2
                break
        while created and top_level > 0:
            achievement, created = Achievement.objects.get_or_create(  # @UnusedVariable
                name=event_type, level=top_level, member=member)
            top_level = top_level / 2
    else:
        return

def achievement_blood_drive(member):
    drives = Attendance.objects.filter(member=member,
        event__description=BLOOD_DRIVE).count()
    create_achievements(drives, TOTAL_BLOOD_DRIVE, member)

def achievement_home_church_host(member):
    hosts = Event.objects.filter(description=HOME_CHURCH, host=member).count()
    create_achievements(hosts, HOME_CHURCH_HOST, member)