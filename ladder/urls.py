from django.conf.urls import *  # @UnusedWildImport @IgnorePep8

urlpatterns = patterns('ladder.views',
    (r'^attendance/(?P<member_id>[-\w]+)/event/(?P<event_id>[-\w]+)/$', 'attendance'),
    (r'^attendance/(?P<member_id>[-\w]+)/$', 'event_selector'),
    (r'^member/(?P<member_id>[-\w]+)/$', 'member'),
)