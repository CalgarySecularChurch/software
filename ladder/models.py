from django.contrib.auth.models import User
from django.db import models


class Attendance(models.Model):

    member = models.ForeignKey(User)
    event = models.ForeignKey('Event')
    wore_shirt = models.BooleanField(default=False)

    class Meta:
        permissions = (
            ("can_take_attendance", "Can take attendance"),
        )

    def __unicode__(self):
        return '%s - %s: %s' % (self.member, self.event, self.event.datetime)


# Name is first letter of each word, with any extra characters taken up by
# continuing with the last word.
ANNUAL_DONATIONS = "ADON"
CONSECUTIVE_SUNDAY_MEETINGS = "CSME"
HOME_CHURCH_HOST = "HCHO"
TOTAL_BLOOD_DRIVE = "TBDR"

ACHIEVEMENTS = (
    (ANNUAL_DONATIONS, "Annual Donations"),
    (CONSECUTIVE_SUNDAY_MEETINGS, "Consecutive Sunday Meetings Attended"),
    (HOME_CHURCH_HOST, "Hosted a Home Church"),
    (TOTAL_BLOOD_DRIVE, "Total Blood Drives Attended"),
)


class Achievement(models.Model):

    name = models.CharField(max_length=4, choices=ACHIEVEMENTS)
    level = models.IntegerField()
    member = models.ForeignKey(User)

    def __unicode__(self):
        return self.get_name_display()

    def image_text(self):
        return '%s%s' % (self.name, self.level)


CAD = "CAD"

CURRENCIES = (
    (CAD, 'Canadian Dollars'),
    )


class Donation(models.Model):

    member = models.ForeignKey(User)
    amount = models.DecimalField(decimal_places=2, max_digits=10)
    currency = models.CharField(max_length=3, choices=CURRENCIES)
    datetime = models.DateTimeField()


BLOOD_DRIVE = "BDRI"
HOME_CHURCH = "HCHU"
SUNDAY_MEETING = "SMEE"

EVENTS = (
    (BLOOD_DRIVE, "Blood Drive"),
    (HOME_CHURCH, "Home Church"),
    (SUNDAY_MEETING, "Sunday Meeting"),
    )

class Event(models.Model):

    description = models.CharField(max_length=4, choices=EVENTS)
    datetime = models.DateTimeField()
    points_per_hour = models.IntegerField()
    hours = models.IntegerField(default=1)
    host = models.ForeignKey(User, blank=True, null=True)

    def __unicode__(self):
        return self.get_description_display()

    def points(self):
        return self.hours * self.points_per_hour


NO_RATING = 0
POOR = 1
ACCEPTABLE = 2
EXCELLENT = 3

RATINGS = (
    (NO_RATING, 'No rating'),
    (POOR, 'Poor'),
    (ACCEPTABLE, 'Acceptable'),
    (EXCELLENT, 'Excellent')
)

class Feedback(models.Model):

    member = models.ForeignKey(User)
    event = models.ForeignKey('Event')
    helpful = models.IntegerField(choices=RATINGS, default=NO_RATING)
    welcoming = models.IntegerField(choices=RATINGS, default=NO_RATING)
    friendly = models.IntegerField(choices=RATINGS, default=NO_RATING)
