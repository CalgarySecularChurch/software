from datetime import datetime
from django.test import TestCase
from django.conf import settings
import pytz
from CalgarySecularChurch.tools import get_time_now
from ladder.models import Achievement, TOTAL_BLOOD_DRIVE, BLOOD_DRIVE,\
    Event, Attendance, SUNDAY_MEETING, CONSECUTIVE_SUNDAY_MEETINGS,\
    ANNUAL_DONATIONS, Donation, CAD
from ladder.views import achievement_blood_drive,\
    achievement_consecutive_sunday_meetings, achievement_donation
from django.contrib.auth.models import User, Permission


class TestAchievements(TestCase):

    def setUp(self):
        User.objects.create_user(username="Member1", password="Password1",
            email='user1@example.com')
        self.member1 = User.objects.get(pk=1)
        self.blood_drive1 = Event.objects.create(description=BLOOD_DRIVE,
            datetime=get_time_now(), points_per_hour=10)
        self.blood_drive2 = Event.objects.create(description=BLOOD_DRIVE,
            datetime=get_time_now(), points_per_hour=10)
        self.blood_drive3 = Event.objects.create(description=BLOOD_DRIVE,
            datetime=get_time_now(), points_per_hour=10)
        self.blood_drive4 = Event.objects.create(description=BLOOD_DRIVE,
            datetime=get_time_now(), points_per_hour=10)
        self.blood_drive5 = Event.objects.create(description=BLOOD_DRIVE,
            datetime=get_time_now(), points_per_hour=10)
        self.meeting1 = Event.objects.create(description=SUNDAY_MEETING,
            datetime=datetime(2013, 10, 3, tzinfo=pytz.timezone(settings.TIME_ZONE)), points_per_hour=10)
        self.meeting2 = Event.objects.create(description=SUNDAY_MEETING,
            datetime=datetime(2013, 10, 4, tzinfo=pytz.timezone(settings.TIME_ZONE)), points_per_hour=10)
        self.meeting3 = Event.objects.create(description=SUNDAY_MEETING,
            datetime=datetime(2013, 10, 5, tzinfo=pytz.timezone(settings.TIME_ZONE)), points_per_hour=10)
        self.meeting4 = Event.objects.create(description=SUNDAY_MEETING,
            datetime=datetime(2013, 10, 6, tzinfo=pytz.timezone(settings.TIME_ZONE)), points_per_hour=10)
        self.meeting5 = Event.objects.create(description=SUNDAY_MEETING,
            datetime=datetime(2013, 10, 7, tzinfo=pytz.timezone(settings.TIME_ZONE)), points_per_hour=10)

    def test_taking_attendance(self):
        p = Permission.objects.get(codename='can_take_attendance')
        self.member1.user_permissions.add(p)
        self.assertTrue(self.client.login(username='Member1', password='Password1'))

        response = self.client.get('/ladder/attendance/%s/' % self.member1.pk)

        self.assertContains(response, 'Blood Drive', status_code=200, count=5)

        response = self.client.post('/ladder/attendance/%s/event/%s/' % (
            self.member1.pk, self.blood_drive1.pk))

        self.assertContains(response, 'http://zxing.appspot.com/scan',
            status_code=200, count=1)
        self.assertEquals(1, len(Attendance.objects.filter(member=self.member1,
            event=self.blood_drive1)))
        self.assertEquals(1, len(Achievement.objects.filter(member=self.member1,
            name=TOTAL_BLOOD_DRIVE)))
        m = User.objects.get(id=self.member1.id)
        print m.profile
        self.assertEquals(self.blood_drive1.points_per_hour, m.profile.total_points)

    def test_QR_code_swipe(self):
        pass

    def test_donation(self):
        achievement_donation(self.member1)

        self.assertEquals(0, len(Achievement.objects.filter(member=self.member1,
            name=ANNUAL_DONATIONS)))

        Donation.objects.create(member=self.member1, amount=1.01,
            datetime=get_time_now(), currency=CAD)

        achievement_donation(self.member1)

        self.assertEquals(1, len(Achievement.objects.filter(member=self.member1,
            name=ANNUAL_DONATIONS)))

        Donation.objects.create(member=self.member1, amount=15.99,
            datetime=get_time_now(), currency=CAD)

        achievement_donation(self.member1)

        self.assertEquals(5, len(Achievement.objects.filter(member=self.member1,
            name=ANNUAL_DONATIONS)))

    def test_consecutive_sunday_meeting(self):
        achievement_consecutive_sunday_meetings(self.member1)

        self.assertEquals(0, len(Achievement.objects.filter(member=self.member1,
            name=CONSECUTIVE_SUNDAY_MEETINGS)))

        Attendance.objects.create(member=self.member1, event=self.meeting1)

        achievement_consecutive_sunday_meetings(self.member1)

        self.assertEquals(1, len(Achievement.objects.filter(member=self.member1,
            name=CONSECUTIVE_SUNDAY_MEETINGS)))

    def test_blood_drive(self):
        achievement_blood_drive(self.member1)

        self.assertEquals(0, len(Achievement.objects.filter(member=self.member1,
            name=TOTAL_BLOOD_DRIVE)))

        Attendance.objects.create(member=self.member1, event=self.blood_drive1)

        achievement_blood_drive(self.member1)

        self.assertEquals(1, len(Achievement.objects.filter(member=self.member1,
            name=TOTAL_BLOOD_DRIVE)))

        Attendance.objects.create(member=self.member1, event=self.blood_drive2)

        achievement_blood_drive(self.member1)

        self.assertEquals(2, len(Achievement.objects.filter(member=self.member1,
            name=TOTAL_BLOOD_DRIVE)))

        Attendance.objects.create(member=self.member1, event=self.blood_drive3)

        achievement_blood_drive(self.member1)

        self.assertEquals(2, len(Achievement.objects.filter(member=self.member1,
            name=TOTAL_BLOOD_DRIVE)))

        Attendance.objects.create(member=self.member1, event=self.blood_drive4)

        achievement_blood_drive(self.member1)

        self.assertEquals(3, len(Achievement.objects.filter(member=self.member1,
            name=TOTAL_BLOOD_DRIVE)))

        Attendance.objects.create(member=self.member1, event=self.blood_drive5)

        achievement_blood_drive(self.member1)

        self.assertEquals(3, len(Achievement.objects.filter(member=self.member1,
            name=TOTAL_BLOOD_DRIVE)))




















