from django.contrib.auth.models import User
from django.test import TestCase
import pytz
from django.conf import settings

from league.models import League, LeagueMember, LOCKED, StatusException, \
    UPDATED, OPEN, CLOSED
from datetime import date


class TestLeague(TestCase):

    def setUp(self):
        for x in range(60):
            User.objects.create_user(username="Member%s" % x,
                password="Password%s" % x, email='user%s@example.com' % x)

    def test_league_creation(self):
        next_year = date.today().year + 1
        for member in User.objects.all():
            League.add_member(member, next_year)

        self.assertEquals(3, len(League.objects.all()))

        new_member = User.objects.create_user(username="NewUser",
            password="NewUser")

        League.add_member(new_member.member, next_year)

        self.assertEquals(4, len(League.objects.all()))
        self.assertEquals(new_member.member, LeagueMember.objects.get(
            league__level=4).member)


class TestEndOfYear(TestCase):

    def setUp(self):
        u1 = User.objects.create_user(username='Member1', password='Password1')
        self.m1 = u1.member
        League.add_member(self.m1, date.today().year)

    def test_member_lock(self):
        LeagueMember.lock_all()

        for lm in LeagueMember.objects.all():
            self.assertEquals(LOCKED, lm.status)

    def test_member_update(self):
        self.assertRaises(StatusException, LeagueMember.update)

        LeagueMember.lock_all()
        LeagueMember.update()

        old_lm = LeagueMember.get(self.m1, date.today().year)
        new_lm = LeagueMember.get(self.m1, date.today().year + 1)
        self.assertEquals(UPDATED, old_lm.status)
        self.assertEquals(date.today().year + 1, new_lm.league.year)
        self.assertEquals(OPEN, new_lm.status)
        self.assertEquals(2, len(LeagueMember.objects.all()))

    def test_member_close(self):
        LeagueMember.lock_all()
        LeagueMember.update()
        LeagueMember.close()

        old_lm = LeagueMember.get(self.m1, date.today().year)
        self.assertEquals(CLOSED, old_lm.status)





























