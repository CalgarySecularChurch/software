from django.contrib.auth.models import User
from django.db import models
from django.utils.datetime_safe import datetime
from datetime import date
import pytz
from django.conf import settings
from CalgarySecularChurch.tools import get_time_now
from voting.models import Campaign


LEAGUE_SIZE = 20

CANADA = 0
NORTH_AMERICA = 1
USA = 2
GLOBAL = 3
GEOGRAPHIC_AREAS = (
    (CANADA, 'Canada'),
    (NORTH_AMERICA, 'North America'),
    (USA, 'USA'),
    (GLOBAL, 'Global'),
)


USER_SELECTED = 0
BRONZE = 1
SILVER = 2
GOLD = 3
PLATINUM = 4
LEVELS = (
    (USER_SELECTED, 'User Selected'),
    (BRONZE, 'Bronze'),
    (SILVER, 'Silver'),
    (GOLD, 'Gold'),
    (PLATINUM, 'Platinum'),
)


class UserProfile(models.Model):
    audit_rating = models.FloatField(default=1)
    campaigns = models.ManyToManyField(Campaign, related_name='individual_campaign', blank=True, null=True)
    country = models.IntegerField(choices=GEOGRAPHIC_AREAS, blank=True, null=True)
    created_on = models.DateTimeField(default=get_time_now)
    hardcore_points = models.IntegerField(default=0)
    total_points = models.IntegerField(default=0)
    user = models.ForeignKey(User, unique=True, related_name='user_profile')

    def add_points(self, points):
        self.total_points += points
        self.hardcore_points += points
        self.save()

    def save(self, *args, **kwargs):
        if not self.pk:
            self.created_on = get_time_now()
        super(UserProfile, self).save(*args, **kwargs)

    def __unicode__(self):
        return str(self.user)


User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])


class Membership(models.Model):

    member = models.ForeignKey(User)
    level = models.IntegerField(choices=LEVELS, default=BRONZE)

    def __unicode__(self):
        return '%s - %s' % (self.user, self.get_level_display())


class League(models.Model):

    country = models.IntegerField(choices=GEOGRAPHIC_AREAS)
    level = models.IntegerField()
    year = models.DecimalField(max_digits=4, decimal_places=0)

    class Meta:
        unique_together = ('country', 'level', 'year')

    def __unicode__(self):
        return '%s %s %s' % (self.level, self.year, self.get_country_display())

    @staticmethod
    def add_member(member, year):
        try:
            last_league = League.objects.filter(year=year).order_by('-level')[0]
        except IndexError:
            last_league = League.objects.create(country=member.country,
                level=1, year=year)
        last_league_members = LeagueMember.objects.filter(league=last_league)
        member_count = len(last_league_members)
        if member_count < LEAGUE_SIZE:
            LeagueMember.objects.create(league=last_league, member=member,
                rank=len(last_league_members) + 1)
        elif member_count == LEAGUE_SIZE:
            new_league = League.objects.create(country=last_league.country,
                level=last_league.level + 1, year = last_league.year)
            LeagueMember.objects.create(league=new_league, member=member,
                rank=1)
        else:
            raise Exception('League %s has too many members!' % last_league)


OPEN = 0
LOCKED = 1
UPDATED = 2
CLOSED = 3

STATUS = (
    (OPEN, 'Open'),
    (LOCKED, 'Locked'),
    (UPDATED, 'Updated'),
    (CLOSED, 'Closed'),
)


class StatusException(Exception):
    pass


class LeagueMember(models.Model):

    league = models.ForeignKey('League')
    member = models.ForeignKey(User)
    rank = models.IntegerField()
    status = models.IntegerField(choices=STATUS, default=OPEN)

    class Meta:
        unique_together = ('league', 'member')

    def __unicode__(self):
        return '%s - %s - %s' % (self.member, self.league, self.rank)

    @staticmethod
    def get(member, year):
        members = LeagueMember.objects.filter(member=member).filter(
                league__year=year)
        assert len(members) == 1
        return members[0]

    @staticmethod
    def get_this_years_league_members():
        return LeagueMember.objects.filter(
            league__year=datetime.today(tzinfo=pytz.timezone(settings.TIME_ZONE)).year).order_by(
                    '-member__total_points')

    @staticmethod
    def lock_all():
        for lm in LeagueMember.get_this_years_league_members():
            if lm.status == OPEN:
                if lm.status != LOCKED:
                    lm.status = LOCKED
                    lm.save()
                else:
                    print "Did not lock %s, as it already has a status of %s" \
                        % (lm, lm.get_status_display())
            else:
                raise StatusException('INVALID STATE: %s had a status of '
                    '%s, but should have been OPEN' % lm.get_status_display())

    @staticmethod
    def update():
        if len(LeagueMember.objects.filter(status__lt=LOCKED, status__gt=LOCKED,
                league__year=date.today(tzinfo=pytz.timezone(settings.TIME_ZONE)).year)) != 0:
            raise StatusException('There are some unlocked LeagueMembers!!')
        for lm in LeagueMember.get_this_years_league_members():
            if lm.status == LOCKED:
                League.add_member(lm.member, date.today(tzinfo=pytz.timezone(settings.TIME_ZONE)).year + 1)
                lm.status = UPDATED
                lm.save()
            else:
                raise StatusException(
                    '%s had the wrong status.' % lm.get_status_display())

    @staticmethod
    def close():
        if len(LeagueMember.objects.filter(status__lt=UPDATED,
                status__gt=UPDATED, league__year=date.today(tzinfo=pytz.timezone(settings.TIME_ZONE)).year)) != 0:
            raise StatusException('There are some LeagueMembers that are not '\
                    'updated!!')
        for lm in LeagueMember.get_this_years_league_members():
            if lm.status == UPDATED:
                lm.status = CLOSED
                lm.save()
            else:
                raise StatusException(
                    '%s had the wrong status.' % lm.get_status_display())



















