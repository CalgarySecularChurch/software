from datetime import datetime

from django.conf import settings
from django.db import models

import pytz


NOT_UPDATED = 0
TEMP_LEAGUE_MEMBER_ROWS_CREATED = 1
FINAL_LEAGUE_MEMBER_ROWS_CREATED = 2
LEAGUE_UPDATE_STATUS = ()


class Administration(models.Model):

    percentage_of_receipts_that_have_been_audited = models.IntegerField(default=0)
    percentage_of_audited_receipts_that_we_have_audited = models.IntegerField(default=0)
    last_time_league_was_reset = models.DateTimeField(default=datetime(year=2000, month=1, day=1, tzinfo=pytz.timezone(settings.TIME_ZONE)))
    percentage_of_members_who_meta_audit = models.IntegerField(default=0)
    league_update_status = models.IntegerField(choices=LEAGUE_UPDATE_STATUS, default=NOT_UPDATED)
    target_audit_percentage = models.FloatField(default=0.1)
    audit_point_total = models.FloatField(default=0)

    def save(self, *args, **kwargs):
        if not (0 < self.target_audit_percentage < 1):
            raise Exception('Must be between 0 and 1')
        super(Administration, self).save(*args, **kwargs)