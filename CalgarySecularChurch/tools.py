from datetime import datetime
import pytz
from django.utils import timezone


def get_time_now():
    return pytz.timezone(timezone.get_default_timezone_name()).localize(
        datetime.now())