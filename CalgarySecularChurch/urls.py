from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^ladder/', include('ladder.urls')),
    url(r'^voting/', include('voting.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
