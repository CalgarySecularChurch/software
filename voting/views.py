from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render_to_response
from django.template.context import RequestContext
from django.views.decorators.cache import never_cache
from CalgarySecularChurch.tools import get_time_now

from voting.models import Campaign, Proposition, Vote, COMPLETE_CHOICES


def all_campaigns(request):
    campaigns = Campaign.objects.all()
    return render_to_response('voting/all_campaigns.html', locals(),
        context_instance=RequestContext(request))


@login_required
@never_cache
def campaign(request, campaign_id):
    campaign = get_object_or_404(Campaign, pk=campaign_id)
    votes = []
    ratings = COMPLETE_CHOICES
    for p in campaign.propositions.all():
        vote, created = Vote.objects.get_or_create(proposition=p, user=request.user)
        votes.append(vote)
    return render_to_response('voting/campaign.html', locals(),
        context_instance=RequestContext(request))


@login_required
def post_vote(request, campaign_id):
    if request.method == 'POST':
        for key, rating in request.POST.items():
            key = key.split('_')
            if key[0] == 'id':
                vote = Vote.objects.get(user=request.user,
                    pk=key[1])
                vote.rating = int(rating)
                vote.vote_date = get_time_now()
                vote.save()
        for proposition in Campaign.objects.get(id=campaign_id).propositions.all():
            proposition.calculate_ratings()
    return render_to_response('voting/thanks.html')


@login_required
def proposition(request, pk):
    proposition = get_object_or_404(Proposition, pk=pk)
    return render_to_response('voting/proposition.html', locals(),
        context_instance=RequestContext(request))


@login_required
def votes(request):
    votes = Vote.objects.filter(user=request.user)
    return render_to_response('voting/votes.html', locals(),
        context_instance=RequestContext(request))