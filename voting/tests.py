from datetime import timedelta

from django.contrib.auth.models import User
from django.test import TestCase
from CalgarySecularChurch.tools import get_time_now
from ladder.models import NO_RATING

from voting.models import Campaign, Proposition, Vote


class TestCampaigns(TestCase):

    def setUp(self):
        self.todays_date = get_time_now()
        self.user1 = User.objects.create_user(
                username='User1', email='user1@example.com',
                password='Password1')
        self.user1.first_name = 'FirstName'
        self.user1.last_name = 'LastName'
        self.user1.save()

        self.user2 = User.objects.create_user(
                username='User2', email='user2@example.com',
                password='Password2')
        self.user2.first_name = 'FirstName'
        self.user2.last_name = 'LastName'
        self.user2.save()

        self.campaign1 = Campaign.objects.create(
                short_description='TestCampaign1',
                start_time=get_time_now() - timedelta(hours=1),
                polls_open=get_time_now() - timedelta(minutes=5),
                end_time=get_time_now() + timedelta(hours=1))
        self.campaign2 = Campaign.objects.create(
                short_description='TestCampaign2',
                start_time=get_time_now() - timedelta(hours=1),
                polls_open=get_time_now() - timedelta(minutes=5),
                end_time=get_time_now() + timedelta(hours=1))

        self.user1.profile.campaigns.add(self.campaign1)
        self.user1.profile.campaigns.add(self.campaign2)
        self.user2.profile.campaigns.add(self.campaign1)
        self.user2.profile.campaigns.add(self.campaign2)

        self.proposition1 = self.create_proposition(self.campaign1, 'Proposition1')
        self.proposition2 = self.create_proposition(self.campaign2, 'Proposition2')

        self.vote1 = Vote.objects.create(user=self.user1, proposition=self.proposition1, rating=50)
        self.vote2 = Vote.objects.create(user=self.user2, proposition=self.proposition2, rating=50)

        self.assertTrue(self.client.login(username='User1',
                password='Password1'))

    def create_proposition(self, campaign, name):
        return Proposition.objects.create(campaign=campaign, short_description=name)

    def test_votes_created_for_each_user_when_proposition_created(self):
        votes = Vote.objects.filter(proposition=self.proposition1)
        self.assertEquals(2, len(votes))
        self.assertEquals(50, votes[0].rating)
        self.assertEquals(NO_RATING, votes[1].rating)

        votes = Vote.objects.filter(proposition=self.proposition2)
        self.assertEquals(2, len(votes))
        self.assertEquals(NO_RATING, votes[0].rating)
        self.assertEquals(50, votes[1].rating)

    def test_votes_created_for_each_proposition_when_user_created(self):
        # TODO:
        pass

    def test_votes_have_a_date(self):
        self.assertAlmostEqual(self.todays_date, self.vote1.vote_date, delta=timedelta(seconds=1))
        self.assertAlmostEqual(self.todays_date, self.vote2.vote_date, delta=timedelta(seconds=1))
        self.assertRaises(Exception, self.campaign1.add_proposition, self.proposition1)

    def test_active_campaigns_accept_votes(self):
        self.proposition1.register_vote(self.vote1)

        self.assertEquals(2, len(self.proposition1.get_all_votes()))

    def test_users_can_see_all_propositions_for_a_campaign(self):
        response = self.client.get('/voting/campaign/%s/' % self.campaign1.pk)

        self.assertContains(response, self.proposition1.short_description,
            count=1, status_code=200)

    def test_propositions_show_their_vote_on_campaign_page(self):
        response = self.client.get('/voting/campaign/%s/' % self.campaign1.pk)

        print response

        self.assertContains(response, 'Currently: %s' % self.vote1.rating, count=1,
            status_code=200)

    def test_users_can_see_details_about_a_proposition(self):
        response = self.client.get('/voting/proposition/%s/' % self.proposition1.pk)

        self.assertContains(response, self.proposition1.short_description,
            count=1, status_code=200)

    def test_users_can_see_all_their_campaigns(self):
        response = self.client.get('/voting/')

        self.assertContains(response, self.campaign1.short_description,
            count=1, status_code=200)
        self.assertContains(response, self.campaign2.short_description,
            count=1, status_code=200)

    def test_users_can_vote_on_a_proposition(self):
        pass

    def test_propositions_know_sum_score(self):
        vote3 = Vote.objects.get(proposition=self.proposition1, user=self.user2)
        vote3.rating = 50
        vote3.save()
        self.proposition1.register_vote(self.vote1)
        self.proposition1.register_vote(vote3)

        self.assertEquals(100, self.proposition1.sum_score)

    def test_porpositions_know_total_votes(self):
        vote3 = Vote.objects.get(proposition=self.proposition1, user=self.user2)
        vote3.rating = 50
        vote3.save()
        self.proposition1.register_vote(self.vote1)
        self.proposition1.register_vote(vote3)

        self.assertEquals(2, self.proposition1.total_votes)
