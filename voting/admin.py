from django.contrib import admin

from voting.models import Campaign, Proposition


class PropositionInline(admin.TabularInline):
    model = Proposition
    extra = 1
    fieldsets = (
        (None, {
            'fields': ('short_description', 'current_result', 'total_votes', 'link', 'long_description'),
        }),
    )
    readonly_fields = ['current_result', 'total_votes']


class CampaignAdmin(admin.ModelAdmin):
    inlines = [PropositionInline, ]


admin.site.register(Campaign, CampaignAdmin)