from django.contrib.auth.models import User
from django.db import models
from CalgarySecularChurch.tools import get_time_now


NO_OPINION = 11
COMPLETE_CHOICES = zip(range(0, 10), range(0, 10))
COMPLETE_CHOICES.append((NO_OPINION, 'NO OPINION'))


class Proposition(models.Model):
    campaign = models.ForeignKey('Campaign', related_name='propositions')
    link = models.URLField(blank=True, null=True)
    long_description = models.TextField(blank=True, null=True)
    short_description = models.CharField(max_length=100)
    current_result = models.IntegerField(default=0)
    total_votes = models.IntegerField(default=0)

    def __unicode__(self):
        return self.short_description

    def get_all_votes(self):
        return Vote.objects.filter(proposition=self)

    def calculate_ratings(self):
        sum_score = 0
        total_votes = 0
        for vote in self.votes.all():
            if vote.rating is not NO_OPINION:
                sum_score += vote.rating
                total_votes += 1
        self.current_result = sum_score // total_votes
        self.total_votes = total_votes
        self.save()

    def save(self, *args, **kwargs):
        super(Proposition, self).save(*args, **kwargs)


class Campaign(models.Model):

    start_time = models.DateTimeField()
    polls_open = models.DateTimeField()
    end_time = models.DateTimeField()
    short_description = models.CharField(max_length=100)

    def __unicode__(self):
        return self.short_description

    def accepting_propositions(self):
        if self.start_time < get_time_now() < self.polls_open:
            return True
        return False

    def add_proposition(self, proposition):
        if self.accepting_propositions():
            proposition.campaign = self
            proposition.save()
        else:
            raise Exception('You cannot add propositions at this time')

    def is_closed(self):
        if get_time_now() > self.end_time:
            return True
        else:
            return False

    def get_all_propositions(self):
        return Proposition.objects.filter(campaign=self)

    def polls_are_open(self):
        if self.polls_open < get_time_now() < self.end_time:
            return True
        return False


class Vote(models.Model):
    rating = models.IntegerField(choices=COMPLETE_CHOICES, default=NO_OPINION)
    proposition = models.ForeignKey('Proposition', related_name='votes')
    vote_date = models.DateTimeField(default=get_time_now)
    user = models.ForeignKey(User)

    class Meta:
        unique_together = (('proposition', 'user'),)

    def __unicode__(self):
        return '%s rated %s at %s' % (self.user, self.proposition, self.get_rating_display())