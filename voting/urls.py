from django.conf.urls import *


urlpatterns = patterns('voting.views',
    (r'^campaign/(?P<campaign_id>[-\w]+)/$', 'campaign'),
    (r'^campaign/(?P<campaign_id>[-\w]+)/vote/$', 'post_vote'),
    (r'^campaigns/$', 'all_campaigns'),
    (r'^proposition/(?P<pk>[-\w]+)/$', 'proposition'),
    (r'^$', 'all_campaigns'),
)