from django.conf.urls import *  # @UnusedWildImport @IgnorePep8

urlpatterns = patterns('proomnibus.views',
    (r'^audit/(?P<member_id>[-\w]+)/$', 'request_audit'),
)