from django.utils.unittest.case import TestCase
from administration.models import Administration
from django.contrib.auth.models import User
from league.models import CANADA


class TestProOmnibus(TestCase):

    def setUp(self):
        user = User.objects.create(username="ProOmnibusUser1", password="Password1")
        user.profile.country = CANADA
        user.profile.save()
        Administration.objects.create()

    def test_audit_score(self):
        self.assertEquals(0, Administration.objects.get().audit_point_total)