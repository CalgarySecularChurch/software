from django.db import models
from django.contrib.auth.models import User
from administration.models import Administration
from league.models import LEVELS


BASE_XP = 100


class Audit(models.Model):

    receipt = models.ForeignKey('Receipt')
    auditor = models.ForeignKey(User)
    datetime = models.DateTimeField()
    meta = models.BooleanField(default=False)
    success = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if not self.id:
            a = Administration.objects.get()
            a.audit_point_total -= 1
            a.save()
        super(Audit, self).save(*args, **kwargs)


class AuditType(models.Model):

    """
    These are things like 'carbon footprint' and 'ethical working conditions'.
    They are things that we are going to audit. They do not contain the exact
    details of how they will be audited, as that could vary every year. Exact
    details will be in the Certification.
    """

    short_description = models.CharField(max_length=100)
    long_description = models.TextField()


class Category(models.Model):

    """
    These are things like 'Politician', 'Toiletries' and 'Petrochemicals'.
    """

    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)


class Certification(models.Model):

#     geographic_area = models.IntegerField(choices=GEOGRAPHIC_AREAS)
    level = models.IntegerField(choices=LEVELS)
    business_category = models.ForeignKey('Category')
    audit_type = models.ForeignKey('AuditType')
    description = models.TextField(blank=True, null=True)
    year = models.DecimalField(decimal_places=0, max_digits=4)


class Customer(models.Model):

    name = models.CharField(max_length=100)


class CustomerCertification(models.Model):

    customer = models.ForeignKey('Customer')
    certification = models.ForeignKey('Certification')
    start_time = models.DateTimeField()
    expiration = models.DateTimeField()


class Receipt(models.Model):

    datetime = models.DateTimeField()
    member = models.ForeignKey(User)
    image = models.ImageField(upload_to='/receipts')
    customer = models.ForeignKey('Customer')

    def save(self, *args, **kwargs):
        if not self.id:
            a = Administration.objects.get()
            a.audit_point_total += (1 * a.target_audit_percentage)
            a.save()
        super(Receipt, self).save(*args, **kwargs)


class ReceiptItem(models.Model):

    receipt = models.ForeignKey('Receipt')
    value = models.DecimalField(decimal_places=2, max_digits=7)
    certification = models.ForeignKey('Certification')